package com.scla.stochos_android.Comment.Model;

import java.util.Date;

/**
 * Created by MartinO on 12/11/2016.
 */
public class Comment {
    private String id;
    private String user_id;
    private String media_id;
    private String content;
    private Date date;
    private Boolean edited;

    public Comment() {
    }

    public Comment(String id, String user_id, String media_id, String content, Date date, Boolean edited) {
        this.id = id;
        this.user_id = user_id;
        this.media_id = media_id;
        this.content = content;
        this.date = date;
        this.edited = edited;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getMedia_id() {
        return media_id;
    }

    public void setMedia_id(String media_id) {
        this.media_id = media_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Boolean getEdited() {
        return edited;
    }

    public void setEdited(Boolean edited) {
        this.edited = edited;
    }
}
