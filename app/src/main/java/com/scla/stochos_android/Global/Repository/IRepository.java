package com.scla.stochos_android.Global.Repository;

/**
 * Created by MartinO on 12/11/2016.
 */

public interface IRepository <I> {
    boolean create(I i);
    I retrieve();
    boolean update(I i);
    boolean delete();
}
