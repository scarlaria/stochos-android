package com.scla.stochos_android.Goal.Repository;

import com.google.firebase.database.DatabaseReference;
import com.scla.stochos_android.Global.Repository.AbstractRepository;
import com.scla.stochos_android.Global.Repository.IRepository;
import com.scla.stochos_android.Goal.Model.Goal;

/**
 * Created by MartinO on 12/11/2016.
 */
public class GoalRepository extends AbstractRepository implements IRepository<Goal> {

    DatabaseReference ref = database.getReference("goal");

    @Override
    public boolean create(Goal goal) {
        ref.setValue("Hello, World!");
        return false;
    }

    @Override
    public Goal retrieve() {
        return null;
    }

    @Override
    public boolean update(Goal goal) {
        return false;
    }

    @Override
    public boolean delete() {
        return false;
    }

}
