package com.scla.stochos_android.Media.Model;

import java.util.Date;

/**
 * Created by MartinO on 12/11/2016.
 */
public class Media {

    private String id;
    private String name;
    private String description;
    private byte [] content;
    private Date upload_date;
    private boolean starred;

}
