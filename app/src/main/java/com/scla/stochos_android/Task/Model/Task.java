package com.scla.stochos_android.Task.Model;

import java.util.Date;

/**
 * Created by MartinO on 12/11/2016.
 */
public class Task {

    private String id;
    private String name;
    private String description;
    private Date start_date;
    private Date end_date;
    private boolean archived;
    private boolean open_visibility;
    private String profileImageURL;
    private boolean starred;

}
